import React from "react";

function InputComponent({ title, register, errors }) {
  console.log(errors);
  return (
    <div className="row">
      <div className="col-3">{title}</div>
      <div className="col-9">
        <input
          className={errors[title] ? "formInvalid" : "formValid"}
          {...register(title, {
            required: `*${title} Required`,
            minLength: {
              value: 5,
              message: `*length should be minimum of 5 Characters`,
            },
          })}
        ></input>
        {errors[title]?.message && (
          <small className="errText">{errors[title]?.message}</small>
        )}
      </div>
    </div>
  );
}

export default InputComponent;
