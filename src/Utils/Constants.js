export const HrStyles = {
  dotted: "dottedHrLine",
  normal: "normalHrLine",
  solid: "solidHrLine",
};

export const InputFocusStyles = {
  normal: "inputNormal",
  error: "inputError",
};

export const ErrorMsgDetails = {
  fromAddress: {
    name: "*Enter Valid Name. Name should only consists of alphabets",
    email: "*Enter Valid Email. examples : abc@gmail.com, hello@co.in",
    address: "*Address Should be of minimum Five Characters",
    cityState: "",
    pincode: "",
    phone: "*Enter Valid Phone Number. Should only consists of Ten digits",
    businessNumber:
      "*Business Number Should be of minimum Six Alphanumeric Characters",
  },
  toAddress: {
    name: "*Enter Valid Name. Name should only consists of alphabets",
    email: "*Enter Valid Email. examples : abc@gmail.com, hello@co.in",
    address: "*Address Should be of minimum Five Characters",
    cityState: "",
    pincode: "",
    phone: "*Enter Valid Phone Number. Should only consists of Ten digits",
  },
  invoiceTitleIsEmpty: "*Enter Invoice Title, Should not be Empty",
  invoiceNumber: {
    invoiceNumber:
      "*Invoice Number Should be of minimum Seven Alphanumeric Characters",
  },
};

export const InitialInvoiceDueDate = (
  invoiceDate = new Date(),
  invoiceTerms = "2 Days"
) => {
  const invoiceDueDate = new Date(invoiceDate);
  const terms = {
    None: 0,
    Custom: 0,
    "Due On Receipt": 0,
    "Next Day": 1,
    "2 Days": 2,
    "3 Days": 3,
    "4 Days": 4,
    "5 Days": 5,
    "6 Days": 6,
    "7 Days": 7,
  };
  invoiceDueDate.setDate(invoiceDueDate.getDate() + terms[invoiceTerms]);
  return invoiceDueDate;
};

export const dateConvertion = (dateObj) => {
  let date = dateObj.getDate().toString()
  let month = dateObj.getMonth() + 1 
  month = month > 9 ? month : '0' + month
  month.toString()
  let year = dateObj.getFullYear().toString()

  return date+'-'+month+'-'+year
}

export const activateGenarateInvoice = (state) => {
  let flag = true;
  for (let fromAddressErr in state.errorDetails.fromAddress) {
    if (!state.errorDetails.fromAddress[fromAddressErr]) {
      flag = false;
      break;
    }
  }
  for (let toAddressErr in state.errorDetails.toAddress) {
    if (!state.errorDetails.toAddress[toAddressErr]) {
      flag = false;
      break;
    }
  }
  for (let invoiceErr in state.errorDetails.invoice) {
    if (!state.errorDetails.invoice[invoiceErr]) {
      flag = false;
      break;
    }
  }
  if (Object.entries(state.itemDetails).length === 0) {
    flag = false;
  }
  for (let lineItemKey in state.errorDetails.invoiceItems) {
    for (let lineItemErr in state.errorDetails.invoiceItems[lineItemKey]) {
      if (!state.errorDetails.invoiceItems[lineItemKey][lineItemErr]) {
        flag = false;
        break;
      }
    }
  }
  return flag;
};
