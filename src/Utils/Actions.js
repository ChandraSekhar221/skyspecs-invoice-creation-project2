
import { actionTypes } from './Contants'

export const createNewInvoice = (payload) => ({
    type: actionTypes.CREATE_INVOICE,
    payload
})

export const editInvoice = (payload) => ({
    type: actionTypes.EDIT_INVOICE,
    payload
})

export const deleteInvoice = (payload) => ({
    type: actionTypes.DELTE_INVOICE,
    payload
})