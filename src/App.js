import './App.css';
import React, { useReducer } from 'react';
import { Route, Routes } from 'react-router-dom'
import Home from './Components/Home';
import PreviewInvoice from './Components/PreviewInvoice';
import EditInvoice from './Components/EditInvoice';
// import InvoicesDetails from './Components/HotTableEx';
import CreateInvoice from './Components/CreateInvoice/Invoice'

import InitialState from './State';
import Reducer from './Reducer';

export const InvoiceStateContext = React.createContext()

function App() {
  const [state, dispatch] = useReducer(Reducer, InitialState);
  return (
    <InvoiceStateContext.Provider value={{ state: state, dispatch: dispatch }} >
      <div className="container">
        <Routes>
          <Route path='/' element={<Home />} ></Route>
          <Route path='/createInvoice' element={<CreateInvoice />} />
          <Route path='/previewInvoice' element={<PreviewInvoice />} >
            <Route index element={<p>Please Select Item</p>} />
            <Route path=':id' element={<PreviewInvoice />} />
          </Route>
          <Route path='/editInvoice' element={<EditInvoice />} >
            <Route path=':id' element={<EditInvoice />} />
          </Route>
          {/* <Route path='/deleteInvoice' element={<DeleteInvoice />} >
            <Route path=':id' element={<DeleteInvoice />} />
          </Route> */}
          <Route path='*' element={<Home />} />
        </Routes>
      </div>
    </InvoiceStateContext.Provider>
  );
}

export default App;
