import React, { useState, useContext } from "react";
import AddressInput from "./AddressInput";
import AddressPopup from "./AddressPopup";
import TextInput from "./TextInput";
import InvoiceDate from "./InvoiceDate";
import InvoiceDueDateTerms from "./InvoiceDueDateTerms";
import ItemDetails from "./ItemDetails";
import SuccessMsgDetails from "./SuccessMsgDetails";
import HorizontalLine from "./HorizontalLine";
import { v4 as uuid } from "uuid";

import LogoDropDown from "./LogoDropDown";

import "./Style.css";

import { InvoiceStateContext } from "../../App";

import { Button, Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import { HrStyles, activateGenarateInvoice } from "../../Utils/Constants";
import { Link, useParams } from "react-router-dom";

function Invoive() {
  const { state, dispatch } = useContext(InvoiceStateContext);
  const [open, setOpen] = useState(false);

  const selectedInvoiceId = useParams().id;
  console.log(selectedInvoiceId);

  let genarateInvoiceActivate = !activateGenarateInvoice(state);

  console.log(state);

  const handleInput = (action) => {
    return dispatch(action);
  };
  const handleDeleteItem = (action) => {
    return dispatch(action);
  };

  const handleCreateItem = (action) => {
    return dispatch(action);
  };

  const handleSubmit = (action) => {
    setOpen(true);
    // return dispatch(action);
  };
  const handleModalSaveInvoiceDetails = () => {
    setOpen(false);
    return dispatch({ type: "saveInvoiceDetails" });
  };
  return (
    <div className="p-2">
      <h2>Invoice Model</h2>
      <hr />
      <section className="container">
        <div className="row" name="Top">
          <div className="col-6 d-flex">
            <TextInput
              value={state.invoiceTitle}
              height="3rem"
              padding="1rem"
              handleEvent={handleInput}
              actionType="invoiceTitile"
              invoiceTitleEmpty={state.errorDetails.invoice.isTitleEmpty}
              invoiceTitleErr={state.errorDetails.invoice.isTitleValid}
            />
          </div>
          <div className="col-6 d-flex justify-content-end">
            <LogoDropDown
              handleEvent={handleInput}
              imageDetails={state.imageDetails}
            />
          </div>
        </div>
        <div className="row pt-5" name="AddressTitle">
          <div className="col-6 d-flex align-items-center">
            <div className="row">
              <div className="col-11">
                <h5 className="m-0 py-1">From</h5>
              </div>
            </div>
          </div>
          <div className="col-6">
            <div className="row">
              <div className="col-11 offset-1 d-flex align-items-center">
                <h5 className="m-0 py-1">Bill To</h5>
              </div>
            </div>
          </div>
        </div>
        <div className="row" name="Address Details">
          <div className="col-6" name="AddressInput">
            <AddressInput
              name="name"
              placeHolder="Enter Name"
              handleEvent={handleInput}
              actionType="fromAddress"
              value={state.from.name || ""}
              textEmptyErr={state.errorDetails.fromAddress.isNameEmpty}
              textValidErr={state.errorDetails.fromAddress.isNameValid}
            />
            <AddressInput
              name="email"
              placeHolder="xyz@gmail.com"
              handleEvent={handleInput}
              actionType="fromAddress"
              value={state.from.email || ""}
              textEmptyErr={state.errorDetails.fromAddress.isEmailEmpty}
              textValidErr={state.errorDetails.fromAddress.isEmailValid}
            />
            <AddressInput
              name="address"
              placeHolder="Enter Address"
              handleEvent={handleInput}
              actionType="fromAddress"
              value={state.from.address || ""}
              textEmptyErr={state.errorDetails.fromAddress.isAddressEmpty}
              textValidErr={state.errorDetails.fromAddress.isAddressValid}
            />
            {state.from.address && (
              <>
                <AddressPopup
                  name="cityState"
                  placeHolder="Enter City, State"
                  handleEvent={handleInput}
                  actionType="fromAddress"
                  value={state.from.cityState || ""}
                  textEmptyErr={state.errorDetails.fromAddress.isCityStateEmpty}
                  textValidErr={state.errorDetails.fromAddress.isCityStateValid}
                />
                <AddressPopup
                  name="pincode"
                  placeHolder="Enter Pincode"
                  handleEvent={handleInput}
                  actionType="fromAddress"
                  value={state.from.pincode || ""}
                  textEmptyErr={state.errorDetails.fromAddress.isPincodeEmpty}
                  textValidErr={state.errorDetails.fromAddress.isPincodeValid}
                />
              </>
            )}
            <AddressInput
              name="phone"
              placeHolder="Enter Phone Number"
              handleEvent={handleInput}
              actionType="fromAddress"
              value={state.from.phone || ""}
              textEmptyErr={state.errorDetails.fromAddress.isPhoneEmpty}
              textValidErr={state.errorDetails.fromAddress.isPhoneValid}
            />
            <AddressInput
              name="businessNumber"
              placeHolder="Enter Business Number"
              handleEvent={handleInput}
              actionType="fromAddress"
              value={state.from.businessNumber || ""}
              textEmptyErr={
                state.errorDetails.fromAddress.isBusinessNumberEmpty
              }
              textValidErr={
                state.errorDetails.fromAddress.isBusinessNumberValid
              }
            />
          </div>
          <div className="col-6" name="toComponent">
            <AddressInput
              name="name"
              placeHolder="Enter Name"
              handleEvent={handleInput}
              actionType="toAddress"
              value={state.to.name || ""}
              textEmptyErr={state.errorDetails.toAddress.isNameEmpty}
              textValidErr={state.errorDetails.toAddress.isNameValid}
            />
            <AddressInput
              name="email"
              placeHolder="xyz@gmail.com"
              handleEvent={handleInput}
              actionType="toAddress"
              value={state.to.email || ""}
              textEmptyErr={state.errorDetails.toAddress.isEmailEmpty}
              textValidErr={state.errorDetails.toAddress.isEmailValid}
            />
            <AddressInput
              name="address"
              placeHolder="Enter Address"
              handleEvent={handleInput}
              actionType="toAddress"
              value={state.to.address || ""}
              textEmptyErr={state.errorDetails.toAddress.isAddressEmpty}
              textValidErr={state.errorDetails.toAddress.isAddressValid}
            />
            {state.to.address && (
              <>
                <AddressPopup
                  name="cityState"
                  placeHolder="Enter City, State"
                  handleEvent={handleInput}
                  actionType="toAddress"
                  value={state.to.cityState || ""}
                  textEmptyErr={state.errorDetails.toAddress.isCityStateEmpty}
                  textValidErr={state.errorDetails.toAddress.isCityStateValid}
                />
                <AddressPopup
                  name="pincode"
                  placeHolder="Enter Pincode"
                  handleEvent={handleInput}
                  actionType="toAddress"
                  value={state.to.pincode || ""}
                  textEmptyErr={state.errorDetails.toAddress.isPincodeEmpty}
                  textValidErr={state.errorDetails.toAddress.isPincodeValid}
                />
              </>
            )}
            <AddressInput
              name="phone"
              placeHolder="Enter Phone Number"
              handleEvent={handleInput}
              actionType="toAddress"
              value={state.to.phone || ""}
              textEmptyErr={state.errorDetails.toAddress.isPhoneEmpty}
              textValidErr={state.errorDetails.toAddress.isPhoneValid}
            />
          </div>
        </div>
        <HorizontalLine type={HrStyles.normal} />
        <div className="row" name="invoiceNumber">
          <div className="col-6">
            <AddressInput
              name="invoiceNumber"
              placeHolder="INV0001"
              handleEvent={handleInput}
              actionType="invoiceNumber"
              value={state.invoiceNumber}
              textEmptyErr={state.errorDetails.invoice.isNumEmpty}
              textValidErr={state.errorDetails.invoice.isNumValid}
            />
          </div>
        </div>
        <div className="row" name="invoiceDate">
          <InvoiceDate
            selected={state.invoiceDate}
            title="Date"
            name="invoiceDate"
            actionType="invoiceDate"
            handleEvent={handleInput}
          />
        </div>
        <div className="row" name="invoiceTerms">
          <InvoiceDueDateTerms
            title="Terms"
            name="invoiceTerms"
            actionType="invoiceTerms"
            handleEvent={handleInput}
          />
          <br></br>
          <br></br>
        </div>
        <div className="row" name="due date">
          {state.invoiceTerms !== "Due On Receipt" &&
            state.invoiceTerms !== "None" && (
              <InvoiceDate
                invoiceTermsValue={state.invoiceTerms}
                selected={state.invoiceDueDate}
                invoiceDate={state.invoiceDate}
                title="Due Date"
                name="invoiceDueDate"
                actionType="invoiceDueDate"
                handleEvent={handleInput}
              />
            )}
        </div>
        <HorizontalLine type={HrStyles.solid} />
        <div className="row pb-2" name="invoiceHeading">
          <div className="col-4 offset-1 d-flex">
            <h6 className="m-0">DESCRIPTION</h6>
          </div>
          <div className="col-2 d-flex justify-content-end">
            <h6 className="m-0">RATE</h6>
          </div>
          <div className="col-2 d-flex  justify-content-end">
            <h6 className="m-0">QTY</h6>
          </div>
          <div className="col-2 d-flex justify-content-end">
            <h6 className="m-0">AMOUNT</h6>
          </div>
          <div className="col-1 d-flex">
            <h6 className="m-0">TAX</h6>
          </div>
        </div>
        <HorizontalLine type={HrStyles.solid} />
        <div name="Alert Message of list Items">
          {!Object.entries(state.itemDetails).length && (
            <div className="row">
              <div className="col d-flex">
                <h5 className="text-danger">
                  * Items Should not be empty. Add one or more items to generate
                  Invoice
                </h5>
              </div>
            </div>
          )}
          {Object.entries(state.itemDetails).map((eachItem) => {
            return (
              <ItemDetails
                key={eachItem[0]}
                item={eachItem}
                handleDelete={handleDeleteItem}
                handleEvent={handleInput}
                errorDetailsOfItems={state.errorDetails.invoiceItems}
              />
            );
          })}
        </div>
        <div className="row pb-2" name="addItemToInvoiceBtn">
          <div className="col-1">
            <button
              className="py-2 px-3"
              style={{ borderRadius: "5px" }}
              onClick={() =>
                handleCreateItem({
                  type: "createItem",
                  payload: {
                    [uuid()]: {
                      description: "",
                      price: "",
                      quantity: "",
                      additionalDetails: "",
                    },
                  },
                })
              }
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                fill="currentColor"
                className="bi bi-plus-square-fill"
                viewBox="0 0 16 16"
              >
                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z" />
              </svg>
            </button>
          </div>
        </div>
        <HorizontalLine type={HrStyles.normal} />
        <div className="row" name="total amounts">
          <div className="col-12">
            <div className="row">
              <div className="col-5 offset-5 d-flex justify-content-end">
                <p>Subtotal</p>
              </div>
              <div className="col-1 d-flex justify-content-end">
                <p>
                  {Object.entries(state.itemDetails)
                    .reduce((prevValue, item) => {
                      let value =
                        parseFloat(item[1].price) *
                        parseFloat(item[1].quantity);
                      if (isNaN(value)) {
                        value = 0;
                      }
                      prevValue += value;
                      return prevValue;
                    }, 0)
                    .toFixed(2)}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="row" name="generateInvoice">
          <div className="col-12 d-flex py-2 justify-content-center align-items-center">
            <button
              className="btn btn-primary"
              disabled={genarateInvoiceActivate}
              onClick={() => handleSubmit({ type: "dataCheck" })}
            >
              {selectedInvoiceId ? "Update Invoice" : "Generate Invoice"}
            </button>
            <Modal
              size="lg"
              isOpen={open}
              fullscreen="md"
              toggle={() => setOpen(false)}
            >
              <ModalHeader toggle={() => setOpen(false)}>
                Invoice Details : {state.invoiceTitle}
              </ModalHeader>
              <ModalBody>
                <SuccessMsgDetails state={state} />
              </ModalBody>
              <ModalFooter>
                <Link to="/">
                  <Button
                    color="primary"
                    onClick={handleModalSaveInvoiceDetails}
                  >
                    Save Invoice Details
                  </Button>
                </Link>{" "}
                <Button
                  onClick={() => {
                    setOpen(false);
                  }}
                >
                  Cancel
                </Button>
              </ModalFooter>
            </Modal>
          </div>
        </div>

        <HorizontalLine type={HrStyles.normal} />
      </section>
    </div>
  );
}

export default Invoive;
