import React from "react";

function HorizontalLine({ type = "dotted" }) {
  return (
    <div className="row">
      <hr  className={type}/>
    </div>
  );
}

export default HorizontalLine;
