import React, { useContext } from "react";
import "handsontable/dist/handsontable.full.css";
// import Handsontable from "handsontable";
import { HotTable, HotColumn } from "@handsontable/react";
import { registerAllModules } from "handsontable/registry";
import { InvoiceStateContext } from "../App";
import { dateConvertion } from "../Utils/Constants";
import EditInvoiceRenderer from "./CRUDButtons/EditInvoiceRenderer";
import DeleteInvoiceRenderer from "./CRUDButtons/DeleteInvoiceRenderer";
import PreviewInvoiceRenderer from "./CRUDButtons/PreviewInvoiceRenderer";


registerAllModules();

function HotTableView() {
  const { state } = useContext(InvoiceStateContext);

  const itemsData = Object.entries(state.finalInvoices).reduce(
    (prevValue, curValue) => {
      let [id, details] = curValue;
      let obj = {
        id: id,
        name: details.invoiceTitle,
        number: details.invoiceNumber,
        date: dateConvertion(details.invoiceDate),
        dueDate: dateConvertion(details.invoiceDueDate),
        quantity: Object.values(details.itemDetails).reduce(
          (preValue, curValue) => {
            console.log(curValue);
            return preValue + parseFloat(curValue.quantity);
          },
          0
        ),
      };
      prevValue.push(obj);
      return prevValue;
    },
    []
  );

  const hotSettings = {
    data: itemsData,
    colHeaders: [
      "Id",
      "Name",
      "Number",
      "Date",
      "DueDate",
      "Quantity",
      "Preview",
      "Edit",
      "Delete",
    ],
    licenseKey: "non-commercial-and-evaluation",
    autoRowSize: true,
    autoColumnSize: true,
  };

  console.log(itemsData);
  console.log(state);

  return (
    <div id="hot-app">
      <HotTable settings={hotSettings}>
        <HotColumn data="id" width={100} />
        <HotColumn data="name" width={100} />
        <HotColumn data="number" width={100} />
        <HotColumn data="date" width={100} />
        <HotColumn data="dueDate" width={100} />
        <HotColumn data="quantity" width={100} />
        <HotColumn data="id" width={100}>
          <PreviewInvoiceRenderer hot-renderer />
        </HotColumn>
        <HotColumn data="id" width={100}>
          <EditInvoiceRenderer hot-renderer />
        </HotColumn>
        <HotColumn data="id" width={100}>
          <DeleteInvoiceRenderer hot-renderer />
        </HotColumn>
      </HotTable>
    </div>
  );
}

export default HotTableView;
