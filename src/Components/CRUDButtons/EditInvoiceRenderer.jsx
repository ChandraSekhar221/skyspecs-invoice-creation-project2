import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";
import { InvoiceStateContext } from "../../App";

function EditInvoiceRenderer({ value }) {
  const { dispatch } = useContext(InvoiceStateContext);

  const handleEditInvoice = () => {
    dispatch({
      type: "editInvoice",
      payload: {
        id: value,
      },
    });
  };

  return (
    <div className="col-12 d-flex py-2 justify-content-center align-items-center">
      <Link to={`/editInvoice/${value}`}>
        <Button
          color="primary"
          children={"Edit"}
          onClick={handleEditInvoice}
        />
      </Link>
    </div>
  );
}

export default EditInvoiceRenderer;
