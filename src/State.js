import { v4 as uuid } from "uuid";

import { InitialInvoiceDueDate } from "./Utils/Constants";

const today = new Date();

const defaultItemId = uuid();
const InitialState = {
  finalInvoices: {},
  invoiceTitle: "INVOICE",
  imageDetails: null,
  from: {},
  to: {},
  invoiceDate: today,
  invoiceNumber: "INV0001",
  invoiceTerms: "Due On Receipt",
  invoiceDueDate: InitialInvoiceDueDate(),
  itemDetails: {
    [defaultItemId]: {

      description: "",
      price: "",
      quantity: "",
      additionalDetails: "",
    },
  },
  errorDetails: {
    fromAddress: {
      isNameEmpty: "",
      isNameValid: "",
      isEmailEmpty: "",
      isEmailValid: "",
      isAddressEmpty: "",
      isAddressValid: "",
      isCityStateEmpty: "",
      isCityStateValid: "",
      isPincodeEmpty: "",
      isPincodeValid: "",
      isPhoneEmpty: "",
      isPhoneValid: "",
      isBusinessNumberEmpty: "",
      isBusinessNumberValid: "",
    },
    toAddress: {
      isNameEmpty: "",
      isNameValid: "",
      isEmailEmpty: "",
      isEmailValid: "",
      isAddressEmpty: "",
      isAddressValid: "",
      isCityStateEmpty: "",
      isCityStateValid: "",
      isPincodeEmpty: "",
      isPincodeValid: "",
      isPhoneEmpty: "",
      isPhoneValid: "",
    },
    invoice: {
      isTitleEmpty: true,
      isTitleValid: true,
      isNumEmpty: true,
      isNumValid: true,
    },
    invoiceItems: {
      [defaultItemId]: {
        isDescriptionEmpty: "",
        isDescriptionValid: "",
        isPriceEmpty: "",
        isPriceValid: "",
        isQuantityEmpty: "",
        isQuantityValid: "",
      },
    },
  },
  editableInvoiceId: '',
};

export default InitialState;
