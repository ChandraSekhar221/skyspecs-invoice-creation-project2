import React, { useContext } from "react";

import { Link } from "react-router-dom";
import { InvoiceStateContext } from "../App";

import { Button } from "reactstrap";
import HotTableView from "./HotTableEx";

function Home() {
  const { state, dispatch } = useContext(InvoiceStateContext);

  const handleCreateInvoice = () => {
    dispatch({
      type: "createInvoice",
    });
  };

  console.log(state);

  return (
    <div className="container">
      <div className="row" name="header">
        <div className="m-4"></div>
        <div className="p-2 d-flex justify-content-center">
          <h3>Welcome to Invoices Page</h3>
        </div>
      </div>
      <div className="row" name="createInvoice">
        <Link to="/createInvoice">
          <Button
            color="primary"
            className="my-2"
            onClick={handleCreateInvoice}
          >
            {" "}
            Create New Invoice{" "}
          </Button>
        </Link>
      </div>
      <div className="row" name="invoiceDetails">
        <HotTableView />
      </div>
      <div className="row p-2" name="emptyInvoiceMessage">
        {Object.keys(state.finalInvoices).length === 0 && (
          <div className="my-2 bg-secondary border rounded text-warning p-2">
            <h5 className="m-0">
              Currently you have no Invoices. Create invoice.
            </h5>
          </div>
        )}
      </div>
    </div>
  );
}

export default Home;
