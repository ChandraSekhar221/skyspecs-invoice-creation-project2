import React, { useContext } from "react";

import { useParams } from "react-router-dom";
import { InvoiceStateContext } from "../App";
import Invoive from "./CreateInvoice/Invoice";

function EditInvoice() {
  const { state } = useContext(InvoiceStateContext);
  let { id } = useParams();
  console.log(id);
  let selectedInvoice = state.finalInvoices[id];
  console.log(selectedInvoice);
  console.log(state);

  return <>{!id ? <p>Plase Select Invoice to edit</p> : <Invoive />}</>;
}

export default EditInvoice;
