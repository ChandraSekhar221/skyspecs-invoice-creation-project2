import React from "react";

import { InputFocusStyles } from "../../Utils/Constants";

function ItemDetails({ item, handleDelete, handleEvent, errorDetailsOfItems }) {
  const style1 = `w-100 py-1 px-2 ${
    errorDetailsOfItems[item[0]]?.isDescriptionEmpty === false ||
    errorDetailsOfItems[item[0]]?.isDescriptionValid === false
      ? InputFocusStyles.error
      : InputFocusStyles.normal
  }`;
  const style2 = `w-100 py-1 px-2 ${
    errorDetailsOfItems[item[0]]?.isPriceValid === false ||
    errorDetailsOfItems[item[0]]?.isPriceEmpty === false
      ? InputFocusStyles.error
      : InputFocusStyles.normal
  }`;
  const style3 = `w-100 py-1 px-2 ${
    errorDetailsOfItems[item[0]]?.isQuantityValid === false ||
    errorDetailsOfItems[item[0]]?.isQuantityEmpty === false
      ? InputFocusStyles.error
      : InputFocusStyles.normal
  }`;
  return (
    <div className="row">
      <div className="col-1">
        <button
          className="py-2 px-3"
          style={{ borderRadius: "5px" }}
          onClick={() =>
            handleDelete({ type: "deleteItem", payload: { id: item[0] } })
          }
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-x"
            viewBox="0 0 16 16"
          >
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
          </svg>
        </button>
      </div>
      <div className="col-4">
        <div className="row">
          <div className="col-12">
            <input
              className={style1}
              type="text"
              placeholder="Item Description"
              value={item[1].description}
              onChange={(e) =>
                handleEvent({
                  type: "itemDescription",
                  payload: { value: e.target.value, id: item[0] },
                })
              }
            ></input>
          </div>
          <div className="col-12 textAlignLeft text-danger">
            {errorDetailsOfItems[item[0]]?.isDescriptionValid === false &&
              errorDetailsOfItems[item[0]]?.isDescriptionEmpty && (
                <small>
                  * Not Valid Description, shoud be minimum Five Characters
                </small>
              )}
            {errorDetailsOfItems[item[0]]?.isDescriptionEmpty === false && (
              <small>* Enter Item Description</small>
            )}
          </div>
        </div>
      </div>
      <div className="col-2">
        <div className="row">
          <div className="col-12">
            <input
              className={style2}
              style={{ textAlign: "right" }}
              placeholder="Price"
              value={item[1].price}
              onChange={(e) =>
                handleEvent({
                  type: "itemPrice",
                  payload: { value: e.target.value, id: item[0] },
                })
              }
            ></input>
          </div>
          <div className="col-12 textAlignLeft text-danger">
            {errorDetailsOfItems[item[0]]?.isPriceValid === false &&
              errorDetailsOfItems[item[0]]?.isPriceEmpty && (
                <small>* Price Should be positive number</small>
              )}
            {errorDetailsOfItems[item[0]]?.isPriceEmpty === false && (
              <small>* Enter Price</small>
            )}
          </div>
        </div>
      </div>
      <div className="col-2">
        <div className="row">
          <div className="col-12">
            <input
              className={style3}
              style={{ textAlign: "right" }}
              placeholder="Qty"
              value={item[1].quantity}
              onChange={(e) =>
                handleEvent({
                  type: "itemQuantity",
                  payload: { value: e.target.value, id: item[0] },
                })
              }
            ></input>
          </div>
          <div className="col-12  textAlignLeft text-danger">
            {errorDetailsOfItems[item[0]]?.isQuantityValid === false &&
              errorDetailsOfItems[item[0]]?.isQuantityEmpty && (
                <small>* Qty Should be positive number</small>
              )}
            {errorDetailsOfItems[item[0]]?.isQuantityEmpty === false && (
              <small>* Enter Quantity</small>
            )}
          </div>
        </div>
      </div>
      <div className="col-2 d-flex justify-content-end align-items-center">
        <p className="m-0">
          ₹{" "}
          {isNaN(
            (parseFloat(item[1].price) * parseFloat(item[1].quantity)).toFixed(2)
          )
            ? "0.00"
            : (parseFloat(item[1].price) * parseFloat(item[1].quantity)).toFixed(2)}
        </p>
      </div>
      <div className="col-1 d-flex align-items-center">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="30"
          height="30"
          fill="white"
          className="bi bi-check2 p-1"
          viewBox="0 0 16 16"
          style={{ backgroundColor: "black", borderRadius: "5px" }}
        >
          <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
        </svg>
      </div>
      <div className="col-4 offset-1 d-felx py-2">
        <textarea
          className="w-100 p-2"
          rows={3}
          placeholder="Additional Details"
          value={item[1].additionalDetails}
          onChange={(e) =>
            handleEvent({
              type: "itemAdditionalDetails",
              payload: { value: e.target.value, id: item[0] },
            })
          }
        ></textarea>
      </div>
      <hr style={{ borderTop: "dotted 1px" }}></hr>
    </div>
  );
}

export default ItemDetails;
