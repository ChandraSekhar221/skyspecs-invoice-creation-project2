import React from "react";

import "./style.css";

import { useForm } from "react-hook-form";

import { addressDetailFileds } from "../Utils/Contants";

function UseReactHookForm() {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  console.log(errors);

  const onChange = (data) => console.log(data);
  const formData = watch();
  console.log(formData); // watch input value by passing the name of it

  return (
    <>
      <form className="container" onSubmit={handleSubmit(onChange)}>
        <div className="row py-2">
          <div className="col-6">
            <div className="row">
              <div className="col-2">
                <label htmlFor="">Name</label>
              </div>
              <div className="col-10">
                <input
                  className={
                    errors[addressDetailFileds.fromName]?.message
                      ? "formInvalid"
                      : "formValid"
                  }
                  {...register(addressDetailFileds.fromName, {
                    required: "Name is Required",
                    minLength: {
                      value: 5,
                      message: "Length should not less than 5",
                    },
                  })}
                />
                {
                  <span className="errText">
                    {errors[addressDetailFileds.fromName]?.message}
                  </span>
                }
              </div>
            </div>
          </div>
          <div className="col-6">
            <div className="row">
              <div className="col-2">
                <label htmlFor="">Name</label>
              </div>
              <div className="col-10">
                <input
                  className={
                    errors[addressDetailFileds.toName]?.message
                      ? "formInvalid"
                      : "formValid"
                  }
                  {...register(addressDetailFileds.toName, {
                    required: "Name is Required",
                    minLength: {
                      value: 5,
                      message: "Length should not less than 5",
                    },
                  })}
                />
                {
                  <span className="errText">
                    {errors[addressDetailFileds.toName]?.message}
                  </span>
                }
                <br />
              </div>
            </div>
          </div>
        </div>
        <div className="row py-2">
          <div className="col-6">
            <div className="row">
              <div className="col-2">
                <label htmlFor="">Email</label>
              </div>
              <div className="col-10">
                <input
                  className={
                    errors[addressDetailFileds.fromEmail]?.message
                      ? "formInvalid"
                      : "formValid"
                  }
                  {...register(addressDetailFileds.fromEmail, {
                    required: "Email is Required",
                    minLength: {
                      value: 5,
                      message: "Length should not less than 5",
                    },
                  })}
                />
                {
                  <span className="errText">
                    {errors[addressDetailFileds.fromEmail]?.message}
                  </span>
                }
                <br />
              </div>
            </div>
          </div>
          <div className="col-6">
            <div className="row">
              <div className="col-2">
                <label htmlFor="">Email</label>
              </div>
              <div className="col-10">
                <input
                  className={
                    errors[addressDetailFileds.toEmail]?.message
                      ? "formInvalid"
                      : "formValid"
                  }
                  {...register(addressDetailFileds.toEmail, {
                    required: "Email is Required",
                    minLength: {
                      value: 5,
                      message: "Length should not less than 5",
                    },
                  })}
                />
                {
                  <span className="errText">
                    {errors[addressDetailFileds.toEmail]?.message}
                  </span>
                }
                <br />
              </div>
            </div>
          </div>
        </div>
        <div className="row py-2">
          <div className="col-6">
            <div className="row">
              <div className="col-2">
                <label htmlFor="">Address</label>
              </div>
              <div className="col-10">
                <input
                  className={
                    errors[addressDetailFileds.fromAddressName]?.message
                      ? "formInvalid"
                      : "formValid"
                  }
                  {...register(addressDetailFileds.fromAddressName, {
                    required: "Address is Required",
                    minLength: {
                      value: 5,
                      message: "Length should not less than 5",
                    },
                  })}
                />
                {
                  <span className="errText">
                    {errors[addressDetailFileds.fromAddressName]?.message}
                  </span>
                }
                <br />
              </div>
            </div>
          </div>
          <div className="col-6">
            <div className="row">
              <div className="col-2">
                <label htmlFor="">Address</label>
              </div>
              <div className="col-10">
                <input
                  className={
                    errors[addressDetailFileds.toAddressName]?.message
                      ? "formInvalid"
                      : "formValid"
                  }
                  {...register(addressDetailFileds.toAddressName, {
                    required: "Address is Required",
                    minLength: {
                      value: 5,
                      message: "Length should not less than 5",
                    },
                  })}
                />
                {
                  <span className="errText">
                    {errors[addressDetailFileds.toAddressName]?.message}
                  </span>
                }
                <br />
              </div>
            </div>
          </div>
        </div>
        <div className="row justify-content-center pt-2">
          <input className="w-auto" type="submit" />
        </div>
      </form>
    </>
  );
}

export default UseReactHookForm;
