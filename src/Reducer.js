import validator from "validator";

import InitialState from "./State";

import { InitialInvoiceDueDate } from "./Utils/Constants";

const Reducer = (state, action) => {
  let isValid, isEmpty;
  switch (action.type) {
    case "invoiceTitile":
      isEmpty = action.payload.value.trim() ? true : false;
      isValid = validator.isAlphanumeric(action.payload.value.trim())
        ? true
        : false;
      return {
        ...state,
        invoiceTitle: action.payload.value.toUpperCase(),
        errorDetails: {
          ...state.errorDetails,
          invoice: {
            ...state.errorDetails.invoice,
            isTitleEmpty: isEmpty,
            isTitleValid: isValid,
          },
        },
      };
    case "dragAndDropImage":
      action.payload["url"] = URL.createObjectURL(action.payload);
      return {
        ...state,
        imageDetails: action.payload,
      };
    case "deleteImage":
      return {
        ...state,
        imageDetails: null,
      };
    case "fromAddress":
      isEmpty = action.payload.value.trim() ? true : false;
      switch (action.payload.name) {
        case "name":
          isValid = validator.isAlpha(action.payload.value.trim())
            ? true
            : false;
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromAddress: {
                ...state.errorDetails.fromAddress,
                isNameEmpty: isEmpty,
                isNameValid: isValid,
              },
            },
          };
        case "email":
          isValid = validator.isEmail(action.payload.value.trim())
            ? true
            : false;
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromAddress: {
                ...state.errorDetails.fromAddress,
                isEmailEmpty: isEmpty,
                isEmailValid: isValid,
              },
            },
          };
        case "address":
          isValid = action.payload.value.trim().length >= 5 ? true : false;
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromAddress: {
                ...state.errorDetails.fromAddress,
                isAddressEmpty: isEmpty,
                isAddressValid: isValid,
              },
            },
          };
        case "cityState":
          isValid = action.payload.value.trim().length >= 5 ? true : false;
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromAddress: {
                ...state.errorDetails.fromAddress,
                isCityStateEmpty: isEmpty,
                isCityStateValid: isValid,
              },
            },
          };
        case "pincode":
          isValid =
            validator.isNumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length === 6
              ? true
              : false;
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromAddress: {
                ...state.errorDetails.fromAddress,
                isPincodeEmpty: isEmpty,
                isPincodeValid: isValid,
              },
            },
          };
        case "phone":
          isValid =
            validator.isNumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length === 10
              ? true
              : false;
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromAddress: {
                ...state.errorDetails.fromAddress,
                isPhoneEmpty: isEmpty,
                isPhoneValid: isValid,
              },
            },
          };
        case "businessNumber":
          isValid =
            validator.isAlphanumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length >= 6
              ? true
              : false;
          return {
            ...state,
            from: {
              ...state.from,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              fromAddress: {
                ...state.errorDetails.fromAddress,
                isBusinessNumberEmpty: isEmpty,
                isBusinessNumberValid: isValid,
              },
            },
          };
        default:
          return {
            ...state,
          };
      }
    case "toAddress":
      isEmpty = action.payload.value.trim() ? true : false;
      switch (action.payload.name) {
        case "name":
          isValid = validator.isAlpha(action.payload.value.trim())
            ? true
            : false;
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toAddress: {
                ...state.errorDetails.toAddress,
                isNameEmpty: isEmpty,
                isNameValid: isValid,
              },
            },
          };
        case "email":
          isValid = validator.isEmail(action.payload.value.trim())
            ? true
            : false;
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toAddress: {
                ...state.errorDetails.toAddress,
                isEmailEmpty: isEmpty,
                isEmailValid: isValid,
              },
            },
          };
        case "address":
          isValid = action.payload.value.trim().length >= 5 ? true : false;
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toAddress: {
                ...state.errorDetails.toAddress,
                isAddressEmpty: isEmpty,
                isAddressValid: isValid,
              },
            },
          };
        case "cityState":
          isValid = action.payload.value.trim().length >= 5 ? true : false;
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toAddress: {
                ...state.errorDetails.toAddress,
                isCityStateEmpty: isEmpty,
                isCityStateValid: isValid,
              },
            },
          };
        case "pincode":
          isValid =
            validator.isNumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length === 6
              ? true
              : false;
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toAddress: {
                ...state.errorDetails.toAddress,
                isPincodeEmpty: isEmpty,
                isPincodeValid: isValid,
              },
            },
          };
        case "phone":
          isValid =
            validator.isNumeric(action.payload.value.trim()) &&
              action.payload.value.trim().length === 10
              ? true
              : false;
          return {
            ...state,
            to: {
              ...state.to,
              [action.payload.name]: action.payload.value,
            },
            errorDetails: {
              ...state.errorDetails,
              toAddress: {
                ...state.errorDetails.toAddress,
                isPhoneEmpty: isEmpty,
                isPhoneValid: isValid,
              },
            },
          };
        default:
          return {
            ...state,
          };
      }
    case "invoiceNumber":
      isEmpty = action.payload.value.trim() ? true : false;
      isValid =
        validator.isAlphanumeric(action.payload.value.trim()) &&
          action.payload.value.trim().length >= 7
          ? true
          : false;
      return {
        ...state,
        invoiceNumber: action.payload.value,
        errorDetails: {
          ...state.errorDetails,
          invoice: {
            ...state.errorDetails.invoice,
            isNumEmpty: isEmpty,
            isNumValid: isValid,
          },
        },
      };
    case "invoiceDate":
      return {
        ...state,
        invoiceDueDate: InitialInvoiceDueDate(
          action.payload.invoiceDate,
          state.invoiceTerms
        ),
        ...action.payload,
      };
    case "invoiceTerms":
      return {
        ...state,
        invoiceDueDate: InitialInvoiceDueDate(
          state.invoiceDate,
          action.payload.invoiceTerms
        ),
        ...action.payload,
      };
    case "invoiceDueDate":
      return {
        ...state,
        ...action.payload,
      };
    case "itemDescription":
      isEmpty = action.payload.value.trim() ? true : false;
      isValid = action.payload.value.trim().length >= 5 ? true : false;
      return {
        ...state,
        itemDetails: {
          ...state.itemDetails,
          [action.payload.id]: {
            ...state.itemDetails[action.payload.id],
            description: action.payload.value
          }
        },
        errorDetails: {
          ...state.errorDetails,
          invoiceItems: {
            ...state.errorDetails.invoiceItems,
            [action.payload.id]: {
              ...state.errorDetails.invoiceItems[[action.payload.id]],
              isDescriptionValid: isValid,
              isDescriptionEmpty: isEmpty,
            },
          },
        },
      };
    case "itemPrice":
      isEmpty = action.payload.value.trim() ? true : false;
      isValid = validator.isNumeric(action.payload.value.trim()) ? true : false;
      return {
        ...state,
        itemDetails: {
          ...state.itemDetails,
          [action.payload.id]: {
            ...state.itemDetails[action.payload.id],
            price: action.payload.value
          }
        },
        errorDetails: {
          ...state.errorDetails,
          invoiceItems: {
            ...state.errorDetails.invoiceItems,
            [action.payload.id]: {
              ...state.errorDetails.invoiceItems[[action.payload.id]],
              isPriceEmpty: isEmpty,
              isPriceValid: isValid,
            },
          },
        },
      };
    case "itemQuantity":
      isEmpty = action.payload.value.trim() ? true : false;
      isValid = validator.isNumeric(action.payload.value.trim()) ? true : false;
      return {
        ...state,
        itemDetails: {
          ...state.itemDetails,
          [action.payload.id]: {
            ...state.itemDetails[action.payload.id],
            quantity: action.payload.value
          }
        },
        errorDetails: {
          ...state.errorDetails,
          invoiceItems: {
            ...state.errorDetails.invoiceItems,
            [action.payload.id]: {
              ...state.errorDetails.invoiceItems[[action.payload.id]],
              isQuantityEmpty: isEmpty,
              isQuantityValid: isValid,
            },
          },
        },
      };
    case "itemAdditionalDetails":
      return {
        ...state,
        itemDetails: {
          ...state.itemDetails,
          [action.payload.id]: {
            ...state.itemDetails[action.payload.id],
            additionalDetails: action.payload.value
          }
        },
      };
    case "deleteItem":
      delete state.errorDetails.invoiceItems[action.payload.id];
      delete state.itemDetails[action.payload.id]
      return {
        ...state,
      };
    case "createItem":
      return {
        ...state,
        itemDetails: { ...state.itemDetails, ...action.payload },
        errorDetails: {
          ...state.errorDetails,
          invoiceItems: {
            ...state.errorDetails.invoiceItems,
            [Object.keys(action.payload)[0]]: {
              isDescriptionEmpty: "",
              isDescriptionValid: "",
              isPriceEmpty: "",
              isPriceValid: "",
              isQuantityEmpty: "",
              isQuantityValid: "",
            },
          },
        },
      };
    case "saveInvoiceDetails":
      let idOfInvoice = Object.keys(state.finalInvoices).length + 1
      if (state.editableInvoiceId !== '') {
        idOfInvoice = state.editableInvoiceId
      }
      return {
        finalInvoices: {
          ...state.finalInvoices,
          [idOfInvoice]: {
            invoiceTitle: state.invoiceTitle,
            imageDetails: state.imageDetails,
            from: state.from,
            to: state.to,
            invoiceDate: state.invoiceDate,
            invoiceNumber: state.invoiceNumber,
            invoiceTerms: state.invoiceTerms,
            invoiceDueDate: state.invoiceDueDate,
            itemDetails: state.itemDetails,
            errorDetails: state.errorDetails,
          },
        },
        invoiceTitle: InitialState.invoiceTitle,
        imageDetails: InitialState.imageDetails,
        from: {},
        to: {},
        invoiceDate: InitialState.invoiceDate,
        invoiceNumber: InitialState.invoiceNumber,
        invoiceTerms: InitialState.invoiceTerms,
        invoiceDueDate: InitialState.invoiceDueDate,
        itemDetails: InitialState.itemDetails,
        errorDetails: InitialState.errorDetails,
        editableInvoiceId: InitialState.editableInvoiceId,
      }

    case "createInvoice":
      console.log('Create Invoice Evet Triggered')
      return {
        ...state,
        invoiceTitle: InitialState.invoiceTitle,
        imageDetails: InitialState.imageDetails,
        from: {},
        to: {},
        invoiceDate: InitialState.invoiceDate,
        invoiceNumber: InitialState.invoiceNumber,
        invoiceTerms: InitialState.invoiceTerms,
        invoiceDueDate: InitialState.invoiceDueDate,
        itemDetails: InitialState.itemDetails,
        errorDetails: InitialState.errorDetails,
        editableInvoiceId: InitialState.editableInvoiceId,
      }
    case "editInvoice":
      console.log('Edit Invoice Evet Triggered')
      console.log(state.finalInvoices[action.payload.id])
      return {
        ...state,
        invoiceTitle: state.finalInvoices[action.payload.id].invoiceTitle,
        imageDetails: state.finalInvoices[action.payload.id].imageDetails,
        from: { ...state.finalInvoices[action.payload.id].from },
        to: { ...state.finalInvoices[action.payload.id].to },
        invoiceDate: state.finalInvoices[action.payload.id].invoiceDate,
        invoiceNumber: state.finalInvoices[action.payload.id].invoiceNumber,
        invoiceTerms: state.finalInvoices[action.payload.id].invoiceTerms,
        invoiceDueDate: state.finalInvoices[action.payload.id].invoiceDueDate,
        itemDetails: { ...state.finalInvoices[action.payload.id].itemDetails },
        errorDetails: { ...state.finalInvoices[action.payload.id].errorDetails },
        editableInvoiceId: action.payload.id,
      }
    case "deleteInvoice":
      delete state.finalInvoices[action.payload.id]
      return {
        ...state,
      }
    default:
      return state;
  }
};

export default Reducer;
