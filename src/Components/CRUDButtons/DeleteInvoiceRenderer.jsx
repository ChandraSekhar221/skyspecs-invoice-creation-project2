import React, { useContext } from "react";
import { Button } from "reactstrap";
import { InvoiceStateContext } from "../../App";

function DeleteInvoiceRenderer({ value }) {
  const { dispatch } = useContext(InvoiceStateContext);

  const handleDeleteInvoice = (id) => {
    dispatch({
      type: "deleteInvoice",
      payload: {
        id: value,
      },
    });
  };

  return (
    <div className="col-12 d-flex py-2 justify-content-center align-items-center">
      <Button
        color="primary"
        children={"Delete"}
        onClick={handleDeleteInvoice}
      />
    </div>
  );
}

export default DeleteInvoiceRenderer;
