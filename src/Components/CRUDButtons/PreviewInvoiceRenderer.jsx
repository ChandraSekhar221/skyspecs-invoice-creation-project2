import React, { useContext, useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { InvoiceStateContext } from "../../App";
import SuccessMsgDetails from "../CreateInvoice/SuccessMsgDetails";

function PreviewInvoiceRenderer({ value }) {
  const { state } = useContext(InvoiceStateContext);
  const [open, setOpen] = useState(false);

  return (
    <div className="col-12 d-flex py-2 justify-content-center align-items-center">
      <Button color="primary" onClick={() => setOpen(true)}>
        Preview
      </Button>
      <Modal
        size="lg"
        isOpen={open}
        fullscreen="md"
        toggle={() => setOpen(false)}
      >
        <ModalHeader toggle={() => setOpen(false)}>Invoice Details</ModalHeader>
        <ModalBody>
          <SuccessMsgDetails state={state} id={value} />
        </ModalBody>
        <ModalFooter>
          <p>Thank You</p>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default PreviewInvoiceRenderer;
