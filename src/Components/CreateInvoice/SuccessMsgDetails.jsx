import React from "react";
import { dateConvertion } from "../../Utils/Constants";

function SuccessMsgDetails({ state, id }) {
  let successData = {
    invoiceNumber: state.invoiceNumber,
    invoiceDate: state.invoiceDate,
    invoiceDueDate: state.invoiceDueDate,
    invoiceTerms: state.invoiceTerms,
    itemDetails: { ...state.itemDetails },
    from: { ...state.from },
    to: { ...state.from },
  };
  if (id) {
    let selectedData = state.finalInvoices[id];
    successData = {
      invoiceNumber: selectedData.invoiceNumber,
      invoiceDate: selectedData.invoiceDate,
      invoiceDueDate: selectedData.invoiceDueDate,
      invoiceTerms: selectedData.invoiceTerms,
      itemDetails: { ...selectedData.itemDetails },
      from: { ...selectedData.from },
      to: { ...selectedData.from },
    };
  }
  console.log(id);
  return (
    <>
      <div
        className="col-12 d-flex py-2"
        style={{ backgroundColor: "lightGray" }}
      >
        <p className="px-3">Invoice Number : {successData.invoiceNumber}</p>
        <p className="px-3">
          Invoice Date :{dateConvertion(successData.invoiceDate)}
        </p>
        <p className="px-3">Invoice terms: {successData.invoiceTerms}</p>
        {!(
          successData.invoiceTerms === "Due On Receipt" ||
          successData.invoiceTerms === "None"
        ) && (
          <p className="px-3">
            Invoice Due date: {dateConvertion(successData.invoiceDueDate)}
          </p>
        )}
      </div>
      <div className="col-12">
        <hr></hr>
      </div>
      <div className="col-12">
        <div className="row">
          <div className="col-1">
            <p>Sl.No</p>
          </div>
          <div className="col-5">
            <p>Description</p>
          </div>
          <div className="col-2">
            <p>Price</p>
          </div>
          <div className="col-2">
            <p>Quantity</p>
          </div>
          <div className="col-2">
            <p>Amount</p>
          </div>
        </div>
        <div className="col-12">
          <hr className="m-0"></hr>
        </div>
        {Object.values(successData.itemDetails).map((item, index) => {
          return (
            <div className="col-12" key={index + 1}>
              <div className="row">
                <div className="col-1">
                  <p>{index + 1}</p>
                </div>
                <div className="col-5">
                  <p>{item.description}</p>
                </div>
                <div className="col-2">
                  <p>{item.price}</p>
                </div>
                <div className="col-2">
                  <p>{item.quantity}</p>
                </div>
                <div className="col-2">
                  <p>
                    ₹{" "}
                    {isNaN(
                      (
                        parseFloat(item.price) * parseFloat(item.quantity)
                      ).toFixed(2)
                    )
                      ? "0.00"
                      : (
                          parseFloat(item.price) * parseFloat(item.quantity)
                        ).toFixed(2)}
                  </p>
                </div>
              </div>
            </div>
          );
        })}
        <div className="col-12">
          <hr></hr>
        </div>
        <div className="col-12">
          <div className="row">
            <div className="col-5 offset-5 d-flex justify-content-end">
              <p>Total</p>
            </div>
            <div className="col-2">
              <p>
                ₹<small> </small>
                {Object.values(successData.itemDetails)
                  .reduce((prevValue, item) => {
                    let value =
                      parseFloat(item.price) * parseFloat(item.quantity);
                    prevValue += value;
                    return prevValue;
                  }, 0)
                  .toFixed(2)}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div
        className="col-12 d-flex flex-column align-items-start py-2"
        name="successData"
      >
        <p className="m-0 pb-1 text-decoration-underline">From Address :</p>
        <small>
          Name : <span className="h6">{successData.from.name}</span>
        </small>
        <small>
          Email : <span className="h6">{successData.from.email}</span>
        </small>
        <small>
          Address :{" "}
          <span className="h6">
            {successData.from.address +
              ", " +
              successData.from.cityState +
              ", " +
              successData.from.pincode}
          </span>
        </small>
        <small>
          Phone Number: <span className="h6">{successData.from.phone}</span>
        </small>
        <small>
          Business Number:{" "}
          <span className="h6">{successData.from.businessNumber}</span>
        </small>
        <p className="m-0 pt-3 pb-1 text-decoration-underline">To Address :</p>
        <small>
          Name : <span className="h6">{successData.to.name}</span>
        </small>
        <small>
          Email : <span className="h6">{successData.to.email}</span>
        </small>
        <small>
          Address :{" "}
          <span className="h6">
            {successData.to.address +
              ", " +
              successData.to.cityState +
              ", " +
              successData.to.pincode}
          </span>
        </small>
        <small>
          Phone Number: <span className="h6">{successData.to.phone}</span>
        </small>
      </div>
    </>
  );
}

export default SuccessMsgDetails;
