import React from "react";

import { InputFocusStyles } from "../../Utils/Constants";

function TextInput({
  type,
  name,
  value,
  height,
  padding,
  actionType,
  handleEvent,
  invoiceTitleErr,
  invoiceTitleEmpty,
}) {
  console.log(value);
  const style = `w-100 py-1 px-2 ${
    invoiceTitleErr === false || invoiceTitleEmpty === false
      ? InputFocusStyles.error
      : InputFocusStyles.normal
  }`;
  return (
    <div>
      <input
        className={style}
        type={type}
        name={name}
        value={value}
        placeholder="Invoice Title"
        onChange={(e) =>
          handleEvent({
            type: actionType,
            payload: { value: e.target.value },
          })
        }
        size="20"
        style={{ height: height, padding: padding }}
      ></input>
      {invoiceTitleErr === false && invoiceTitleEmpty && (
        <small className="d-flex text-danger">
          *Title Should only consits of Alphanumeric chars.
        </small>
      )}
      {invoiceTitleEmpty === false && (
        <small className="d-flex text-danger">*Enter Invoice Title.</small>
      )}
    </div>
  );
}

export default TextInput;
